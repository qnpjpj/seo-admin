import request from '@/utils/request'

// 图片上传
export function uploadImg(data) {
  return request({
    url: '/api.v1/admin/upload/img',
    method: 'POST',
    data
  })
}
