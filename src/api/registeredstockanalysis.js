import request from '@/utils/request'

// 注册存量分析查询
export function stock(data) {
  return request({
    url: '/device/stock/query',
    method: 'post',
    data
  })
}
