import request from '@/utils/request'

// 主查询接口
export function onlineQuery(data) {
  return request({
    url: '/device/online/query',
    method: 'post',
    data
  })
}

// 时间轴详情
export function onlineDetail(data) {
  return request({
    url: '/device/online/detail',
    method: 'post',
    data
  })
}

// 车辆及车主信息（用户信息）
export function vehicleInfo(data) {
  return request({
    url: '/vehicle/info',
    method: 'post',
    data
  })
}

// 近7日异常统计
export function onlineSevenDayStat(data) {
  return request({
    url: '/device/online/sevenDayStat',
    method: 'post',
    data
  })
}

// 车辆轨迹
export function getTrackPlayback(data) {
  return request({
    url: '/vehicle/trace',
    method: 'post',
    data
  })
}

// 区域统计
export function onlineAreaStat(data) {
  return request({
    url: '/device/online/areaStat',
    method: 'post',
    data
  })
}
