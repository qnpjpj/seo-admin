import request from '@/utils/request'
// 三包核查申请
export function getDateApply(data) {
  return request({
    url: 'engine/guarantee/apply',
    method: 'post',
    data
  })
}

// 保存服务商信息到核查档案
export function getDateApplyItem1(data) {
  return request({
    url: 'engine/guarantee/apply/item1',
    method: 'post',
    data
  })
}

// 保存发动机与车主信息到核查档案
export function getDateApplyItem2(data) {
  return request({
    url: 'engine/guarantee/apply/item2',
    method: 'post',
    data
  })
}

// 查询报修前的姿态告警信息
export function getDateAccident(data) {
  return request({
    url: 'engine/guarantee/query/accident',
    method: 'post',
    data
  })
}

// 保存姿态信息到核查档案
export function getDateApplyItem3(data) {
  return request({
    url: 'engine/guarantee/apply/item3',
    method: 'post',
    data
  })
}

// 查询报修前的异常故障信息
export function getDateAlarm(data) {
  return request({
    url: 'engine/guarantee/query/alarm',
    method: 'post',
    data
  })
}

// 保存异常故障信息到核查档案
export function getDateApplyItem5(data) {
  return request({
    url: 'engine/guarantee/apply/item5',
    method: 'post',
    data
  })
}

// 三包核查预览
export function getDateView(data) {
  return request({
    url: 'engine/guarantee/view',
    method: 'post',
    data
  })
}

// 查询报修后的轨迹信息
export function getDateTrace(data) {
  return request({
    url: 'engine/guarantee/query/trace',
    method: 'post',
    data
  })
}

// 保存轨迹截图到核查档案
export function getDateQueryItem4(data) {
  return request({
    url: 'engine/guarantee/apply/item4',
    method: 'post',
    data
  })
}
