import request from '@/utils/request'

// 企业管理列表
export function managementList(data) {
  return request({
    url: '/list',
    method: 'POST',
    data
  })
}

// 删除企业
export function companyDel(data) {
  return request({
    url: '/del',
    method: 'POST',
    data
  })
}
