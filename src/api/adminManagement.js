import request from '@/utils/request'

// 添加管理员
export function addAdmin(data) {
  return request({
    url: '/api.v1/admin/admin/add',
    method: 'POST',
    data
  })
}

// 管理员列表
export function adminList(data) {
  return request({
    url: '/api.v1/admin/admin/list',
    method: 'GET',
    params: data
  })
}

// 删除管理员
export function delAdmin(data) {
  return request({
    url: '/api.v1/admin/admin/del',
    method: 'POST',
    data
  })
}
