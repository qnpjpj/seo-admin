import request from '@/utils/request'

// 用户列表
export function customersList(data) {
  return request({
    url: '/api.v1/admin/user/list',
    method: 'GET',
    params: data
  })
}

// 用户考试
export function enginePushOrder(data) {
  return request({
    url: '/api.v1/admin/user/exam/list',
    method: 'post',
    data
  })
}
