import request from '@/utils/request'

// 主查询接口（包括列表与地图
export function hydraulicQuery(data) {
  return request({
    url: '/hydraulic/fault/query',
    method: 'post',
    data
  })
}

// 推送任务
export function hydraulicPushOrder(data) {
  return request({
    url: 'hydraulic/fault/pushOrder',
    method: 'post',
    data
  })
}

// 统计任务（包括列表与地图））
export function hydraulicStatOrder(data) {
  return request({
    url: '/hydraulic/fault/statOrder',
    method: 'post',
    data
  })
}

// 故障统计
export function hydraulicOrderStat(data) {
  return request({
    url: '/hydraulic/fault/statOrder2',
    method: 'post',
    data
  })
}

// 列表查询
export function hydraulicQueryOrder(data) {
  return request({
    url: '/hydraulic/fault/queryOrder',
    method: 'post',
    data
  })
}
