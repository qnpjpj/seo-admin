import request from '@/utils/request'

// 试卷列表
export function testPaper(params) {
  return request({
    url: '/api.v1/admin/testPaper/list',
    method: 'get',
    params
  })
}

// 获取企业下所有岗位
export function allPosition(params) {
  return request({
    url: '/api.v1/admin/company/all/position',
    method: 'get',
    params
  })
}

// 添加试卷接口
export function addPaper(data) {
  return request({
    url: '/api.v1/admin/testPaper/add',
    method: 'post',
    data
  })
}

// 删除试卷
export function delPaper(data) {
  return request({
    url: '/api.v1/admin/testPaper/del',
    method: 'post',
    data
  })
}

// 试卷上下架
export function checkPaper(data) {
  return request({
    url: '/api.v1/admin/testPaper/check/status',
    method: 'post',
    data
  })
}

// 添加试题
export function addQuestions(data) {
  return request({
    url: '/api.v1/admin/examQuestions/add',
    method: 'post',
    data
  })
}

// 试题列表
export function questionsList(params) {
  return request({
    url: '/api.v1/admin/examQuestions/list',
    method: 'get',
    params
  })
}

// 保存试卷
export function saveTest(data) {
  return request({
    url: '/api.v1/admin/testPaper/save',
    method: 'post',
    data
  })
}

// 试卷信息
export function infoTest(params) {
  return request({
    url: '/api.v1/admin/testPaper/info',
    method: 'GET',
    params
  })
}

// 试卷信息
export function delQuestion(data) {
  return request({
    url: '/api.v1/admin/examQuestions/del',
    method: 'POST',
    data
  })
}

// 保存试题
export function saveQuestion(data) {
  return request({
    url: '/api.v1/admin/examQuestions/save',
    method: 'POST',
    data
  })
}
