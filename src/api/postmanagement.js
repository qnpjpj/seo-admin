import request from '@/utils/request'

// 添加岗位
export function positionAdd(data) {
  return request({
    url: '/api.v1/admin/position/add',
    method: 'POST',
    data
  })
}

// 岗位列表
export function positionList(data) {
  return request({
    url: '/api.v1/admin/position/list',
    method: 'GET',
    params: data
  })
}

// 删除岗位
export function positionDel(data) {
  return request({
    url: '/api.v1/admin/position/del',
    method: 'post',
    data
  })
}

// 保存岗位
export function positionSave(data) {
  return request({
    url: '/api.v1/admin/position/save',
    method: 'post',
    data
  })
}
