import request from '@/utils/request'

// 主查询接口
export function outputQuery(data) {
  return request({
    url: '/output/online/query',
    method: 'post',
    data
  })
}

// 推送任务
export function onlinePushOrder(data) {
  return request({
    url: '/output/online/pushOrder',
    method: 'post',
    data
  })
}

// 查询任务（包括列表与地图））
export function outputQueryOrder(data) {
  return request({
    url: '/output/online/queryOrder',
    method: 'post',
    data
  })
}

// 统计分析
export function onlineOrderStat(data) {
  return request({
    url: '/output/online/orderStat',
    method: 'post',
    data
  })
}
