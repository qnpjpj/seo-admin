import request from '@/utils/request'

// Banner列表
export function bannerList(params) {
  return request({
    url: '/api.v1/admin/banner/list',
    method: 'get',
    params
  })
}

// Banner删除
export function bannerDel(params) {
  return request({
    url: '/api.v1/admin/banner/del',
    method: 'post',
    params
  })
}

// 添加Banner
export function addBanner(data) {
  return request({
    url: '/api.v1/admin/banner/add',
    method: 'post',
    data
  })
}

// 保存banner
export function saveBanner(data) {
  return request({
    url: '/api.v1/admin/banner/save',
    method: 'POST',
    data
  })
}
