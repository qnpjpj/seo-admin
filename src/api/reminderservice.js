import request from '@/utils/request'

// 发动机定保统计
export function warnStat(data) {
  return request({
    url: '/warn/maintain/engine/stat',
    method: 'post',
    data
  })
}

// 发动机定保列表
export function warnEngineList(data) {
  return request({
    url: '/warn/maintain/engine/list',
    method: 'post',
    data
  })
}

// 液压定保列表
export function warnMaintainList(data) {
  return request({
    url: '/warn/maintain/hyd/list',
    method: 'post',
    data
  })
}

// 提醒规则
export function warnRule(data) {
  return request({
    url: '/warn/rule',
    method: 'post',
    data
  })
}

// 液压定保统计
export function warnWarnStat(data) {
  return request({
    url: '/warn/maintain/hyd/stat',
    method: 'post',
    data
  })
}

// 尿素提醒统计
export function warnUreaStat(data) {
  return request({
    url: '/warn/urea/stat',
    method: 'post',
    data
  })
}

// 尿素定保列表
export function warnUreaList(data) {
  return request({
    url: '/warn/urea/list',
    method: 'post',
    data
  })
}
