import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import 'tailwindcss/tailwind.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/color.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n

import '@/styles/index.scss' // global css
import '@/assets/icon/iconfont.css'

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

// f-ui
import FCard from './lib/Card/Card'
import FButton from './lib/Button/Button'

import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

Vue.component('f-card', FCard)
Vue.component('f-button', FButton)

// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
