import { positionList } from '@/api/postmanagement'
import { setStore, getStore } from '@/utils/tools'

const state = {
  usage: JSON.parse(getStore('Usage')) || [],
  tableData: [],
  pagePost: {},
  editData: {}
}

const mutations = {
  SET_MALL: (state, data) => {
    state.usage = data
    setStore('Usage', data)
  },

  SET_LIST: (state, data) => {
    state.tableData = data
    setStore('TableData', data)
  },

  SET_PAGEPOST: (state, data) => {
    state.pagePost = data
    setStore('Post', data)
  },

  SET_EDIT: (state, data) => {
    state.editData = data
  }
}

const actions = {
  positionList({ commit }, data) {
    return new Promise((resolve, reject) => {
      positionList(data).then(({ code, data: { list }, data: { pageInfo } }) => {
        if (code === 200) {
          console.log(pageInfo)
          const arr = []
          let obj = {}
          for (let i = 0; i < list.length; i++) {
            obj = {
              ...list[i].info,
              ...list[i].companyInfo
            }
            arr.push(obj)
          }
          commit('SET_LIST', arr)
          commit('SET_PAGEPOST', pageInfo)
          resolve()
        }
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
