import { outputQuery, outputQueryOrder, onlineOrderStat } from '@/api/onlinemonitoring'
import { setStore, getStore, getNowFormatDate } from '@/utils/tools'

const state = {
  monData: '',
  engineData: '', // 推送数量
  change: 'table-display', // 切换模式
  change2: 'query-table-Display',
  switchText: '地图展示', // 切换文字
  stats: JSON.parse(getStore('Stats')) || [], // 统计分析
  hour: '',
  type: 0
}

const mutations = {
  SET_TYPE: (state, type) => {
    state.type = type
    setStore('Type', type)
  },

  SET_MONDATA: (state, monData) => {
    state.monData = monData
    setStore('Mondata', monData)
  },

  SET_ENGINEDATA: (state, engineData) => {
    state.engineData = engineData
    setStore('EngineData', engineData)
  },

  SET_CHANGE: (state, change) => {
    state.change = change
    setStore('Change', change)
  },

  SET_CHANGES: (state, change2) => {
    state.change2 = change2
    setStore('Change2', change2)
  },

  SET_SWITCH: (state, switchText) => {
    state.switchText = switchText
    setStore('SwitchText', switchText)
  },

  SET_STATS: (state, stats) => {
    state.stats = stats
    setStore('Stats', stats)
  },

  SET_HOUR: (state, hour) => {
    state.hour = hour
    setStore('Hour', hour)
  }
}

const actions = {
  // 主查询接口
  outputQuery({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate(),
      areaId: '' || 100000,
      usageId: '' || 0

    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      outputQuery(obj).then(({ resCode, rows, total }) => {
        if (resCode === 200) {
          const monData = { rows, total }
          commit('SET_MONDATA', monData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 查询任务（包括列表与地图）
  outputQueryOrder({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate(),
      areaId: '' || 100000,
      hour: '' || 24
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      outputQueryOrder(obj).then(({ resCode, rows, total, areaCount }) => {
        if (resCode === 200) {
          const monData = { rows, total, areaCount }
          commit('SET_MONDATA', monData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 统计分析
  onlineOrderStat({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)

    return new Promise((resolve, reject) => {
      onlineOrderStat(obj).then(({ resCode, stats }) => {
        if (resCode === 200) {
          commit('SET_STATS', stats)
          commit('SET_HOUR', obj.hour)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
