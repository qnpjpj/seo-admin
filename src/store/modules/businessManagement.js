import { managementList } from '@/api/businessManagement'
import { setStore, getStore } from '@/utils/tools'

const state = {
  companyList: [] || JSON.parse(getStore('companyList')),
  companyInfo: {} || JSON.parse(getStore('companyInfo')),
  bussPage: {}
}

const mutations = {
  SET_LIST: (state, companyList) => {
    state.companyList = companyList
    setStore('companyList', companyList)
  },

  SET_INFO: (state, companyInfo) => {
    state.companyInfo = companyInfo
    setStore('companyInfo', companyInfo)
  },

  SET_BUSPAGE: (state, areaName) => {
    state.bussPage = areaName
  }
}

const actions = {
  // 企业列表 companyAdd
  managementList({ commit }, data) {
    return new Promise((resolve, reject) => {
      managementList(data).then(({ code, data: { list }, data: { pageInfo } }) => {
        if (code === 200) {
          commit('SET_LIST', list)
          commit('SET_BUSPAGE', pageInfo)
        }
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
