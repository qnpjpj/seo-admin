import { customersList } from '@/api/customersmanagement'

const state = {
  alarmList: [],
  custPage: {}
}
const mutations = {
  SET_LIST: (state, data) => {
    state.alarmList = data
    // setStore('Change', change)
  },
  SET_CUST: (state, data) => {
    state.custPage = data
  }
}

const actions = {
  // 用户列表
  customersList({ commit }, data) {
    return new Promise((resolve, reject) => {
      customersList(data)
        .then(({ code, data: { list }, data: { pageInfo }}) => {
          if (code === 200) {
            let obj = {}
            const arr = []
            for (let i = 0; i < list.length; i++) {
              obj = { ...list[i].userBaseInfo, ...list[i].userInfo }
              arr.push(obj)
            }
            commit('SET_LIST', arr)

            commit('SET_CUST', pageInfo)
          }
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
