import { addAdmin, adminList } from '@/api/adminManagement'
import { Message } from 'element-ui'
const state = {
  sData: [],
  resData: []

}

const mutations = {
  SET_MONDATA: (state, resData) => {
    state.sData = resData
  },

  SET_RESDATA: (state, resData) => {
    state.resData = resData
  },

  SET_MONTH: (state, offlineMonth) => {
    state.offlineMonth = offlineMonth
  },

  SET_DEVICE: (state, deviceType) => {
    state.deviceType = deviceType
  },

  SET_DETAIL: (state, resData) => {
    state.detailTableData = resData
  }

}

const actions = {
  // 管理员列表
  adminList({ commit }, data) {
    return new Promise((resolve, reject) => {
      adminList(data).then(({ code, data }) => {
        if (code === 200) {
          const resData = data.list
          commit('SET_MONDATA', resData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 添加管理员
  addAdmin({ commit }, data) {
    return new Promise((resolve, reject) => {
      addAdmin(data).then(({ code, msg }) => {
        if (code === 200) {
          Message({
            message: '创建成功！',
            type: 'success',
            duration: 5 * 1000
          })
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
