import { testPaper, allPosition, questionsList } from '@/api/testmanagement'
import { setStore } from '@/utils/tools'
import { Message } from 'element-ui'
const state = {
  testData: [],
  PData: [],
  QList: [],
  QListPage: {},
  tListPage: {},
  testInfo: {}
}
const mutations = {
  SET_MONDATA: (state, list) => {
    state.testData = list
    setStore('Testdata', list)
  },

  SET_TESTINFO: (state, testInfo) => {
    state.testInfo = testInfo
    setStore('TestInfo', testInfo)
  },

  SET_POSITION: (state, data) => {
    state.PData = data
    setStore('Pdata', data)
  },

  SET_QUESTION: (state, data) => {
    state.QList = data
    setStore('QList', data)
  },

  SET_TESTPAGE: (state, data) => {
    state.tListPage = data
    setStore('TListPage', data)
  }
}

const actions = {
  // 主查询接口
  testPaper({ dispatch, commit }, data) {
    return new Promise((resolve, reject) => {
      testPaper(data)
        .then(({ code, data: { list }, data: { pageInfo }}) => {
          if (code === 200) {
            let obj = {}
            const arr = []
            for (let i = 0; i < list.length; i++) {
              obj = { ...list[i].companyInfo, ...list[i].info }
              obj.on = 20
              arr.push(obj)
            }
            commit('SET_MONDATA', arr)
            commit('SET_TESTPAGE', pageInfo)
          }
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // 企业下所有岗位
  allPosition({ commit }, data) {
    return new Promise((resolve, reject) => {
      allPosition(data)
        .then(({ code, data }) => {
          if (code === 200) {
            if (data === null) {
              Message({
                message: '请先创建岗位',
                type: 'error'
              })
              return
            } else {
              commit('SET_POSITION', data)
            }
          }
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // 试题列表
  questionsList({ commit }, data) {
    return new Promise((resolve, reject) => {
      questionsList(data)
        .then(({ code, data: { list }}) => {
          if (code === 200) {
            if (list.length === 0) {
              Message({
                message: '请先添加试题',
                type: 'error'
              })
              return
            } else {
              let obj = {}
              for (let i = 0; i < list.length; i++) {
                const arr = []
                const cont = JSON.parse(list[i].content)
                for (const key in cont) {
                  obj = {
                    outanswer: key,
                    outcome: cont[key]
                  }
                  arr.push(obj)
                }
                list[i].cont = arr
                list[i].radio = ''
                list[i].checked = []
              }
              commit('SET_QUESTION', list)
            }
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
