// import { setStore, getStore } from '@/utils/tools'

const state = {
  change3: 'table-display', // 切换模式
  switchText3: '地图展示' // 切换文字
}

const mutations = {
  SET_CHANGE: (state, change) => {
    state.change3 = change
    // setStore('Change', change)
  },
  SET_SWITCH: (state, switchText) => {
    state.switchText3 = switchText
    // setStore('SwitchText', switchText)
  }
}

const actions = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
