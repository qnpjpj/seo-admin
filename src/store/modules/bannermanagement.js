import { bannerList } from '@/api/bannermanagement'
import { setStore } from '@/utils/tools'

const state = {
  bannanerData: [],
  bannanerPage: {}
}

const mutations = {
  SET_DATATYPE: (state, list) => {
    state.bannanerData = list
    setStore('DataType', list)
  },

  SET_BANNERPAGE: (state, pageInfo) => {
    state.bannanerPage = pageInfo
    setStore('DataType', pageInfo)
  }
}

const actions = {
  // 主查询接口
  bannerList({ commit }, data) {
    return new Promise((resolve, reject) => {
      bannerList(data)
        .then(({ code, data: { list }, data: { pageInfo }}) => {
          if (code === 200) {
            commit('SET_DATATYPE', list)
            commit('SET_BANNERPAGE', pageInfo)
          }
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
