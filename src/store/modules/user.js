import { login, logout } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import {
  setStore,
  getStore,
  getNowFormatDate,
  removeStore
} from '@/utils/tools'
import { resetRouter } from '@/router'

const state = {
  token: getToken(),
  name: getStore('FDuser') || '',
  staffId: '' || getStore('StaffId'),
  avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
    setToken(token)
  },
  SET_NAME: (state, name) => {
    state.name = name
    setStore('FDuser', name)
  },
  SET_STAFFID: (state, staffId) => {
    state.staffId = staffId
    setStore('StaffId', staffId)
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      const obj = {
        userName: username.trim(),
        passWord: password
      }
      login(obj)
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      const obj = {
        reqId: Math.ceil(Math.random() * 10000),
        reqTime: getNowFormatDate()
      }
      console.log(obj)
      logout(obj)
        .then(res => {
          commit('SET_TOKEN', '')
          removeToken()
          resetRouter()
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken()
      removeStore()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
