import { warnStat, warnEngineList, warnRule, warnWarnStat, warnMaintainList, warnUreaStat, warnUreaList } from '@/api/reminderservice'
import { setStore, getStore, getNowFormatDate } from '@/utils/tools'

const state = {
  datas: [],
  detailData: [],
  rules: [] || JSON.parse(getStore('DetailData'))
}

const mutations = {
  SET_DATAS: (state, datas) => {
    state.datas = datas
    setStore('Mondata', datas)
  },

  SET_DETAILDATA: (state, detailData) => {
    state.detailData = detailData
    setStore('DetailData', detailData)
  },

  SET_RULES: (state, rules) => {
    state.rules = rules
    setStore('Rules', rules)
  },

  SET_CHANGES: (state, change2) => {
    state.change2 = change2
    setStore('Change2', change2)
  },

  SET_SWITCH: (state, switchText) => {
    state.switchText = switchText
    setStore('SwitchText', switchText)
  },

  SET_STATS: (state, stats) => {
    state.stats = stats
    setStore('Stats', stats)
  },

  SET_HOUR: (state, hour) => {
    state.hour = hour
    setStore('Hour', hour)
  }
}

const actions = {
  // 液压主查询接口
  warnWarnStat({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnWarnStat(obj).then(({ resCode, datas }) => {
        if (resCode === 200) {
          commit('SET_DATAS', datas)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 主查询接口
  warnStat({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnStat(obj).then(({ resCode, datas }) => {
        if (resCode === 200) {
          commit('SET_DATAS', datas)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 尿素主查询接口
  warnUreaStat({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnUreaStat(obj).then(({ resCode, datas }) => {
        if (resCode === 200) {
          commit('SET_DATAS', datas)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 发动机查询任务（
  warnEngineList({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnEngineList(obj).then(({ resCode, datas, total }) => {
        if (resCode === 200) {
          const detailData = { datas, total }
          commit('SET_DETAILDATA', detailData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 液压查询任务（
  warnMaintainList({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnMaintainList(obj).then(({ resCode, datas, total }) => {
        if (resCode === 200) {
          const detailData = { datas, total }
          commit('SET_DETAILDATA', detailData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 尿素查询任务
  warnUreaList({ commit }, data) {
    const datas = data
    let obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }
    obj = Object.assign(obj, datas)
    return new Promise((resolve, reject) => {
      warnUreaList(obj).then(({ resCode, datas, total }) => {
        if (resCode === 200) {
          const detailData = { datas, total }
          commit('SET_DETAILDATA', detailData)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 提醒规则
  warnRule({ commit }) {
    const obj = {
      reqId: Math.ceil(Math.random() * 10000),
      reqTime: getNowFormatDate()
    }

    return new Promise((resolve, reject) => {
      warnRule(obj).then(({ resCode, rule1, rule2, rule3 }) => {
        if (resCode === 200) {
          const rules = [rule1, rule2, rule3]
          commit('SET_RULES', rules)
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
