import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import common from './modules/common'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import testmanagement from './modules/testmanagement'
import businessManagement from './modules/businessManagement'
import customersmanagement from './modules/customersmanagement'
import postmanagement from './modules/postmanagement'
import bannermanagement from './modules/bannermanagement' // 排放在线监控
import adminManagement from './modules/adminManagement' // 管理员管理

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    testmanagement,
    businessManagement,
    customersmanagement,
    postmanagement,
    bannermanagement,
    common,
    adminManagement
  },
  getters

})

export default store
