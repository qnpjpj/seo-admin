const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  companyList: state => state.businessManagement.companyList, // 企业管理列表
  bussPage: state => state.businessManagement.bussPage, // 企业管理列表分页
  companyInfo: state => state.businessManagement.companyInfo, // 企业管理列表
  sData: state => state.adminManagement.sData, // 管理员列表
  alarmList: state => state.customersmanagement.alarmList, // 用户列表
  usage: state => state.postmanagement.usage, // 全部企业
  tableData: state => state.postmanagement.tableData, // 岗位列表
  pagePost: state => state.postmanagement.pagePost, // 岗位列表分页
  editData: state => state.postmanagement.editData, //
  testData: state => state.testmanagement.testData, // 试卷列表
  PData: state => state.testmanagement.PData, // 企业下所有岗位
  QList: state => state.testmanagement.QList, // 试题列表
  QListPage: state => state.testmanagement.QListPage,
  tListPage: state => state.testmanagement.tListPage, // 试卷列表分页
  testInfo: state => state.testmanagement.testInfo,
  bannanerData: state => state.bannermanagement.bannanerData, // banner列表
  bannanerPage: state => state.bannermanagement.bannanerPage, // banner列表分页
  custPage: state => state.customersmanagement.custPage // banner列表分页
}
export default getters
