/**
 * 时间戳函数.
 */

export function getNowFormatDate() {
  var date = new Date()
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var strDate = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  if (month >= 1 && month <= 9) {
    month = '0' + month
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = '0' + strDate
  }
  if (hour >= 0 && hour <= 9) {
    hour = '0' + hour
  }
  if (minute >= 0 && minute <= 9) {
    minute = '0' + minute
  }
  if (second >= 0 && second <= 9) {
    second = '0' + second
  }
  var currentdate = year + month + strDate + hour + minute + second
  return currentdate
}

/**
 * 存储localStorage
 */
const setStore = (name, content) => {
  if (!name) return false
  typeof content !== 'string' && (content = JSON.stringify(content))
  window.localStorage.setItem(name, content)
  return true
}
/**
 * 获取localStorage
 */
const getStore = (name) => {
  if (!name) return false
  return window.localStorage.getItem(name) || false
}
/**
 * 删除localStorage
 */
const removeStore = (name) => {
  if (!name) return false
  window.localStorage.removeItem(name)
  return true
}
/**
 * 存储sessionStorage
 */
const setSession = (name, content) => {
  if (!name) return false
  if (typeof content !== 'string') {
    content = JSON.stringify(content)
  }
  window.sessionStorage.setItem(name, content)
  return true
}
/**
 * 获取sessionStorage
 */
const getSession = (name) => {
  if (!name) return false
  return window.sessionStorage.getItem(name) || false
}
/**
 * 删除sessionStorage
 */
const removeSession = (name) => {
  if (!name) return false
  window.sessionStorage.removeItem(name)
  return true
}
/**
 * 默认七天时间
 */
function timeForMat(count) {
  // 拼接时间
  const time1 = new Date()
  time1.setTime(time1.getTime() - 24 * 60 * 60 * 1000)
  const Y1 = time1.getFullYear()
  const M1 =
    time1.getMonth() + 1 > 10
      ? time1.getMonth() + 1
      : '0' + (time1.getMonth() + 1)
  const D1 = time1.getDate() > 10 ? time1.getDate() : '0' + time1.getDate()
  const timer1 = Y1 + '-' + M1 + '-' + D1 // 当前时间
  const time2 = new Date()
  time2.setTime(time2.getTime() - 24 * 60 * 60 * 1000 * count)
  const Y2 = time2.getFullYear()
  const M2 =
    time2.getMonth() + 1 > 9
      ? time2.getMonth() + 1
      : '0' + (time2.getMonth() + 1)
  const D2 = time2.getDate() > 9 ? time2.getDate() : '0' + time2.getDate()
  const timer2 = Y2 + '-' + M2 + '-' + D2 // 之前的7天或者30天
  return [timer2, timer1]
  // return Y2 + '-' + M2 + '-' + D2 + ' 00:00:00'
}

/**
 * 校验是否是正整数
 */

const checkIsNumber = (val) => {
  const reg = /^[0-9]*[0-9][0-9]*$/
  return reg.test(val)
}

/**
 * 判断是否是邮箱地址
 * @param {String} data
 */
const checkEmail = data => {
  const reg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g
  if (reg.test(data)) return true
}

/**
 * 判断是否是手机号，只要是13,14,15,16,17,18,19开头即可
 * @param {String} data
 */
const checkTelphone = data => {
  const reg = /^((\+|00)86)?1[3-9]\d{9}$/g
  if (reg.test(data)) return true
}

/**
 * 判断是否是正确的网址
 * @param {String} url 网址
 */
const checkUrl = url => {
  const a = document.createElement('a')
  a.href = url
  return [
    /^(http|https):$/.test(a.protocol),
    a.host,
    a.pathname !== url,
    a.pathname !== `/${url}`
  ].find(x => !x) === undefined
}

/**
 * 判断字符是否包含某值
 * @param {String} str 字符
 * @param {String} value 字符
 */
const strInclude = (str, value) => {
  return str.includes(value)
}

/**
 *  获取 url 后面通过?传参的参数
 * @param {String} name
 */
const getQueryString = (name) => {
  const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  const url = window.location.href
  const search = url.substring(url.lastIndexOf('?') + 1)
  const r = search.match(reg)
  if (r != null) return unescape(r[2])
  return null
}

/**
 *  比较两个对象中公共的值是否相等
 * @param {Object} ob1
 * @param {Object} ob2
 */
const isSameObject = (ob1, ob2) => {
  for (const key of Object.keys(ob1)) { // 遍历对象的key
    if (ob1[key] !== ob2[key]) { return false }
    return true
  }
}

/**
 *  遍历对象key-value
 * @param {Object} ob
 * @param {Array} arr
 */
const traversalObject = (arr, ob) => {
  let obj = {}
  for (const key in ob) {
    obj = {
      prop: key,
      label: ob[key]
    }
    arr.push(obj)
  }
  return arr
}

// GPS坐标转换高德地图

const wgs84togcj02 = (lng, lat) => {
  // eslint-disable-next-line no-unused-vars
  var x_PI = (3.14159265358979324 * 3000.0) / 180.0
  var PI = 3.1415926535897932384626
  var a = 6378245.0
  var ee = 0.00669342162296594323
  // eslint-disable-next-line no-redeclare
  var lat = +lat
  // eslint-disable-next-line no-redeclare
  var lng = +lng

  var out_of_china = function out_of_china(lng, lat) {
    // eslint-disable-next-line no-redeclare
    var lat = +lat
    // eslint-disable-next-line no-redeclare
    var lng = +lng
    // 纬度3.86~53.55,经度73.66~135.05
    return !(lng > 73.66 && lng < 135.05 && lat > 3.86 && lat < 53.55)
  }

  var transformlat = function transformlat(lng, lat) {
    // eslint-disable-next-line no-redeclare
    var lat = +lat
    // eslint-disable-next-line no-redeclare
    var lng = +lng
    var ret = -100.0 +
      2.0 * lng +
      3.0 * lat +
      0.2 * lat * lat +
      0.1 * lng * lat +
      0.2 * Math.sqrt(Math.abs(lng))
    ret +=
      ((20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) *
        2.0) /
      3.0
    ret +=
      ((20.0 * Math.sin(lat * PI) + 40.0 * Math.sin((lat / 3.0) * PI)) *
        2.0) /
      3.0
    ret +=
      ((160.0 * Math.sin((lat / 12.0) * PI) +
        320 * Math.sin((lat * PI) / 30.0)) *
        2.0) /
      3.0
    return ret
  }

  var transformlng = function transformlng(lng, lat) {
    // eslint-disable-next-line no-redeclare
    var lat = +lat
    // eslint-disable-next-line no-redeclare
    var lng = +lng
    var ret =
      300.0 +
      lng +
      2.0 * lat +
      0.1 * lng * lng +
      0.1 * lng * lat +
      0.1 * Math.sqrt(Math.abs(lng))
    ret +=
      ((20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) *
        2.0) /
      3.0
    ret +=
      ((20.0 * Math.sin(lng * PI) + 40.0 * Math.sin((lng / 3.0) * PI)) *
        2.0) /
      3.0
    ret +=
      ((150.0 * Math.sin((lng / 12.0) * PI) +
        300.0 * Math.sin((lng / 30.0) * PI)) *
        2.0) /
      3.0
    return ret
  }

  if (out_of_china(lng, lat)) {
    return [lng, lat]
  } else {
    var dlat = transformlat(lng - 105.0, lat - 35.0)
    var dlng = transformlng(lng - 105.0, lat - 35.0)
    var radlat = (lat / 180.0) * PI
    var magic = Math.sin(radlat)
    magic = 1 - ee * magic * magic
    var sqrtmagic = Math.sqrt(magic)
    dlat = (dlat * 180.0) / (((a * (1 - ee)) / (magic * sqrtmagic)) * PI)
    dlng = (dlng * 180.0) / ((a / sqrtmagic) * Math.cos(radlat) * PI)
    var mglat = lat + dlat
    var mglng = lng + dlng
    return [mglng, mglat]
  }
}

export {
  wgs84togcj02,
  strInclude,
  checkUrl,
  checkTelphone,
  setStore,
  getStore,
  removeStore,
  setSession,
  getSession,
  removeSession,
  checkIsNumber,
  timeForMat,
  checkEmail,
  getQueryString,
  isSameObject,
  traversalObject
}
