const purgecss = require('@fullhuman/postcss-purgecss')({
  // Specify the paths to all of the template files in your project
  content: [
    './public/**/*.html',
    './src/**/*.vue',
    './src/**/*.scss',
    './src/**/*.css',
    './node_modules/element-ui/lib/theme-chalk/*.css'
  ],
  css: [],
  whitelist: [],
  whitelistPatterns: [],
  whitelistPatternsChildren: [],
  variables: true,
  rejected: true,
  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
})

module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss')('./tailwind.config.js'),
    require('autoprefixer'),
    ...(process.env.NODE_ENV === 'production' ? [purgecss] : [])
    // ...[purgecss]
  ]
}
